import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addStock } from '../actions'
import { AddStock } from '../components/AddStock'
import { StockList } from '../components/StockList'

class StockApp extends React.Component {
  render() {
    return (
      <div>
        <AddStock handleClick={this.props.onClick} />
        <StockList data={this.props.text} />
      </div>
    )
  }
}

StockApp.propTypes = {
  onClick: React.propTypes.func.isRequired,
  text: React.propTypes.string
}

const mapStateToProps = state => ({
  text: state.text
})

const mapDispatchToProps = dispatch => ({
  onClick: text => dispatch(addStock(text))
})

export const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StockApp)
