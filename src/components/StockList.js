import React from 'react'
import PropTypes from 'prop-types'

class StockList extends React.Component {
  render() {
    return (
      <div>{this.props.data}</div>
    )
  }
}

StockList.propTypes = {
  data: React.PropTypes.string
}

export default StockList
