import React from 'react'
import PropTypes from 'prop-types'

class AddStock extends React.Component {
  addStock(e) {
    e.preventDefault()
    this.props.handleClick(this.newItem.text.trim())
    this.newItem.text = ''
    return
  }
  render() {
    return (
      <form>
        <input type='text' ref={(ref) => (this.newItem = ref)} defaultValue='' />
        <button onClick={(event) => this.addStock(event)}>登録</button>
      </form>
    )
  }
}

AddStock.propTypes = {
  handleClick: React.PropTypes.func.isRequired
}

export default AddStock
