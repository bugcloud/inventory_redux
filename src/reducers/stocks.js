const stocks = (state, action) => {
  switch (action.type) {
    case 'ADD_STOCK':
      return [
        ...state,
        {
          text: action.text
        }
      ]
    default:
      return state
  }
}

export default stocks
